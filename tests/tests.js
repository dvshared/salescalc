const assert = require('assert');
const calculateCost = require('../index').calculateCost;

describe('Tests ', () => {
    // Calculate total cost
    it('Input: items 500, price 1.00, state ON / Output: 565.00', () => {
        assert.equal(calculateCost(500, 1, 'ON'), 565.00);
    })

    it('Input: items 3600, price 2.25, state MI / Output: 7984.98', () => {
        assert.equal(calculateCost(3600, 2.25, 'MI'), 7984.98);
    })

    // Item quantity validations
    it('Item quantity is undefined or null', () => {
        const expectedError = new Error('Invalid item quantity: null');
        assert.throws(() => {
            calculateCost(null, 1, 'ON');
        }, expectedError);
    })

    it('Item quantity not a number (NAN)', () => {
        const expectedError = new Error('Item quantity is NaN: zzz');
        assert.throws(() => {
            calculateCost('zzz', 1, 'ON')
        }, expectedError);
    })

    it('Item quantity is less or equal to 0 ', () => {
        const expectedError = new Error('Item quantity must be greater than zero: 0');
        assert.throws(() => {
            calculateCost(0, 1, 'ON')
        }, expectedError);
    })

    // Item price validations
    it('Item price is undefined or null', () => {
        const expectedError = new Error('Invalid item price: null');
        assert.throws(() => {
            calculateCost(500, null, 'ON');
        }, expectedError);
    })

    it('Item price is not a number (NAN)', () => {
        const expectedError = new Error('Item price is NaN: zzz');
        assert.throws(() => {
            calculateCost(500, 'zzz', 'ON')
        }, expectedError);
    })

    it('Item price is less or equal to 0', () => {
        const expectedError = new Error('Item price must be greater than zero: -1');
        assert.throws(() => {
            calculateCost(500, -1, 'ON')
        }, expectedError);
    })

    // State/Province code validations
    it('State/Province code is undefined or null', () => {
        const expectedError = new Error('Invalid state or province code: null');
        assert.throws(() => {
            calculateCost(500, 1, null)
        }, expectedError);
    })

    it('State/Province code is invalid', () => {
        const expectedError = new Error('Invalid state or province code: ONTARIO. Expected: AB, ON, QC, MI, DE');
        assert.throws(() => {
            calculateCost(500, 1, 'ONTARIO')
        }, expectedError);
    })
})
