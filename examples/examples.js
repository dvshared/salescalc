'use strict';

const calculateCost = require('../index').calculateCost;

const itemQty = 500;
const itemPrice = 1;
const stateCode = 'ON';


// ## Example 1
console.log('\nInput:  500 items, $1 per item, Ontario');

let cost = calculateCost(itemQty, itemPrice, stateCode);
console.log(`Output: $${cost}`);

// ## Example 2
console.log('\nInput:  3600 items, $2.25 per item, Michigan');

cost = calculateCost(3600, 2.25, 'MI');
console.log(`Output: $${cost}`);