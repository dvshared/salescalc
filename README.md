# Sales cost calculator for JavaScript

## Requirements
Node.js 8.17+
NPM 6.13.4+

Mocha 7.1.2 (will be installed as a devDependency)

## Installation

For the latest version as a local library
Simply run the following command from within a folder of your choosing

```sh
$ git clone https://bitbucket.org/dvshared/salescalc.git
$ cd salescalc
$ npm install
```

Or from an existing project as a NPM module

```sh
$ npm install @unsignedc/salescalc
```

## Programmatic Access

```js
'use strict';

const calculateCost = require('../index').calculateCost;

const itemQty = 500;
const itemPrice = 1;
const stateCode = 'ON';

let totalCost = calculateCost(itemQty, itemPrice, stateCode);
console.log('Total cost for 500 items at $1.00 in Ontario is', `$${totalCost}`);

totalCost = calculateCost(3600, 2.25, 'MI');
console.log('Total cost for 3600 items at $2.25 in Michigan is', `$${totalCost}`);
```

## Tests

```sh
$ npm test
```

### Tests output

```sh
> mocha tests/tests.js

  Tests
    √ Input: items 500, price 1.00, state ON / Output: 565.00
    √ Input: items 3600, price 2.25, state MI / Output: 7984.98
    √ Item quantity is undefined or null
    √ Item quantity not a number (NAN)
    √ Item quantity is less or equal to 0
    √ Item price is undefined or null
    √ Item price is not a number (NAN)
    √ Item price is less or equal to 0
    √ State/Province code is undefined or null
    √ State/Province code is invalid


  10 passing (11ms)
```

## Description

# Pricing Problem

We have a network of vendors who re-sell our products. We wish to provide them an application to calculate the total cost of an order.

The app needs to give volume discounts and include sales tax.

Another system will accept input from the user, and will call this component with 3 inputs:

* number of items
* price per item
* 2-letter province/state code

The application should output the total price.

The total price is calculated by:

* calculate the total cost for the items
* deduct discount based on the quantity
* add sales tax based on the province/state code

The following tables give the discount rate and tax rates:

| Order Value | Discount Rate |
| ------------- |-------------:|
| $1,000        | 3% |
| $5,000        | 5% |
| $7,000        | 7% |
| $10,000       | 10% |

| Province | Tax Rate |
| ------------- |-------------:|
| AB | 5% |
| ON | 13% |
| QC | 14.975% |
| MI | 6% |
| DE | 0% |

## Examples:

```sh
$ npm run examples
```

### Example 1:

    Input:  500 items, $1 per item, Ontario
    Output: $565.00

### Example 2:

    Input:  3600 items, $2.25 per item, Michigan
    Output: $7984.98

## License

ISC

[npm-url]: https://npmjs.org/package/salecalc
[downloads-url]: https://npmjs.org/package/salescalc
