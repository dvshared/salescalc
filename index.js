'use strict';

/** Tax rate from province/state code */
const taxRates = {
    AB: 0.05,
    ON: 0.13,
    QC: 0.14975,
    MI: 0.06,
    DE: 0,
};

/**
 * Find discount rate
 * @param {Integer} cost
 * @returns {Integer} the result - discount rate
 */
const findDiscountRate = (cost) => {
    if (cost < 1000)
        return 0;

    if (cost >= 1000 && cost < 5000)
        return 0.03;

    if (cost >= 5000 && cost < 7000)
        return 0.05;

    if (cost >= 7000 && cost < 10000)
        return 0.07;

    // >= 10000 return 10%
    return 0.1;
};

/**
 * Calculate Total Cost
 * @param {Integer} itemQty
 * @param {Float} itemPrice
 * @param {String} stateCode
 * @returns {Float} total cost result as Fixed(2)(cost - discount + tax)
 * @throws Error if validation failed
 */
const calculateCost = (itemQty, itemPrice, stateCode) => {
    if (typeof itemQty === "undefined" || itemQty === null)
        throw new Error(`Invalid item quantity: ${itemQty}`);

    if (isNaN(itemQty))
        throw new Error(`Item quantity is NaN: ${itemQty}`);

    if (itemQty <= 0)
        throw new Error(`Item quantity must be greater than zero: ${itemQty}`);


    if (typeof itemPrice === "undefined" || itemPrice === null)
        throw new Error(`Invalid item price: ${itemPrice}`);

    if (isNaN(itemPrice))
        throw new Error(`Item price is NaN: ${itemPrice}`);

    if (itemPrice <= 0)
        throw new Error(`Item price must be greater than zero: ${itemPrice}`);


    if (typeof stateCode === "undefined" || stateCode === null)
        throw new Error(`Invalid state or province code: ${stateCode}`);


    const taxRate = taxRates[stateCode.trim()];

    if (typeof taxRate === "undefined")
        throw new Error(`Invalid state or province code: ${stateCode}. Expected: AB, ON, QC, MI, DE`);


    const cost = itemQty * itemPrice;

    const discountRate = findDiscountRate(cost);

    return (cost * (1 - discountRate) * (1 + taxRate)).toFixed(2);
};

module.exports.calculateCost = calculateCost;